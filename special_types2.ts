let w:unknown = 1;
w = "string";
w ={
    runANonExistentMethod: () => {
        console.log("I think therefore i am");
    }
} as{ runANonExistentMethod: () => void}

if(typeof w=== 'object' && w!== null){
    (w as { runANonExistentMethod: Function}).runANonExistentMethod();
}